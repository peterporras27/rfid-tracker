<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->id();
            $table->string('first_name')->nullalbe();
            $table->string('last_name')->nullalbe();
            $table->string('middle_name')->nullalbe();
            $table->integer('age')->nullalbe();
            $table->string('gender')->nullalbe();
            $table->string('address')->nullalbe();
            $table->string('phone')->nullalbe();
            $table->string('rfid')->nullalbe();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
};
