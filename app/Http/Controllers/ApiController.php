<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Establishment;
use App\Models\Member;
use App\Models\Log;

class ApiController extends Controller
{
    public function index(Request $request) 
    {   
        // Data to be returned
        $return = [
            'error' => true,
            'message' => 'Please try again'
        ];

        // Fetch the sent RFID
        $rfid = $request->input('rfid');
        // Fetch the sent Device ID
        $deviceid = $request->input('device_id');

        // Check if establishment exist
        $establishment = Establishment::where('device_id','=',$deviceid)->first();
        if (!$establishment) {
            $return['message'] = 'Establishment not registered';
            return response()->json( $return );            
        }

        // Check if memeber exists / register
        $member = Member::where('rfid','=',$rfid)->first();
        if (!$member) {
            $return['message'] = 'Member not registered';
            return response()->json( $return );            
        }        

        // Save log if member and establishment exists.
        $log = new Log;
        $log->name = $member->first_name.' '.$member->last_name;
        $log->rfid = $rfid;
        $log->establishment_id = $establishment->id;
        $log->member_id = $member->id;
        $log->save();

        // Return success message and status.
        $return['error'] = false;
        $return['message'] = 'Thank you '.$member->first_name.' '.$member->last_name;
        return response()->json( $return );  
    }
}
