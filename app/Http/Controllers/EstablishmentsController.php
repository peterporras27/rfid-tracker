<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Establishment;
use App\Models\Log;

class EstablishmentsController extends Controller
{
    public function __construct()
    {
        // Reject non authenticated users.
        // Redirect them to login page.
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        // Fetch all establishments and show 10 items per page.
        $establishments = Establishment::paginate(8);

        // Display html layuout for establishment homepage.
        return view('establishments.index', compact('establishments')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('establishments.create'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate the submitted information
        request()->validate([
            'name' => 'required|string',
            'address' => 'required|string',
            'phone' => 'required|string',
            'device_id' => 'required|string'
        ]);

        // Save Information to database.
        $establishment = new Establishment;
        $establishment->fill($request->all());
        $establishment->save();

        // Return to dahsboard with success message.
        return redirect('/dashboard')->with('success','Establishment successfully added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        // find establishment with specific ID
        $establishment = Establishment::find($id);

        // Pull all logs partaining to establishment.
        $logs = Log::where('establishment_id',$id);

        // Check if filter is available.
        if ( $request->input('from') && $request->input('to') ) {
            // Search logs between specified dates.
            $logs = $logs->whereBetween('created_at', [$request->input('from'), $request->input('to')]);
        }

        // Show 20 logs per page.
        $logs = $logs->paginate(20);

        // Display logs.
        return view('establishments.show', compact('establishment','logs')); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $establishment = Establishment::find($id);

        return view('establishments.edit', compact('establishment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Validate submitted information.
        request()->validate([
            'name' => 'required|string',
            'address' => 'required|string',
            'phone' => 'required|string',
            'device_id' => 'required|string'
        ]);

        // Find existing Establishment in database and update.
        $establishment = Establishment::find($id);
        $establishment->fill($request->all());
        $establishment->save();

        // Redirect with successful message.
        return redirect('/establishments/'.$id.'/edit')->with('success','Establishment successfully saved!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Find establishment with specific ID.
        $estab = Establishment::find($id);
        $estab->delete(); // Delete establishment.

        // Redirect with successful message.
        return redirect('/dashboard')->with('success','Establishment successfully removed!');
    }

    public function print(Request $request, $id)
    {
        // find establishment with specific ID
        $establishment = Establishment::find($id);

        // Pull all logs partaining to establishment.
        $logs = Log::where('establishment_id',$id);

        $title = '';
        // Check if filter is available.
        if ( $request->input('from') && $request->input('to') ) {
            // Search logs between specified dates.
            $logs = $logs->whereBetween('created_at', [$request->input('from'), $request->input('to')]);
            $title = 'From: '.date('F j, Y',strtotime($request->input('from')));
            $title .= ' To: '.date('F j, Y',strtotime($request->input('to')));
        }

        // Show 20 logs per page.
        $logs = $logs->get();



        // Display logs.
        return view('print', compact('establishment','logs','title'));
    }
}
