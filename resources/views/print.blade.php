<!doctype html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <title>Print</title>
    </head>
    <body>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col mt-5">
                    @if($logs->count())

                        @if(!empty($title))
                        <h5>{{ $title }}</h5>
                        @endif
                        
                        <table id="print" class="table table-bordered table-striped table-hover table-sm">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Date</th>
                                    <th>Time</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach( $logs as $log )
                                <tr>
                                    <td>
                                        @if($log->member)
                                            {{ $log->member->first_name.' '.$log->member->middle_name.' '.$log->member->last_name }}
                                        @endif
                                    </td>
                                    <td>{{ $log->created_at->format('F j, Y') }}</td>
                                    <td>{{ $log->created_at->format('g:i a') }}</td>
                                 </tr>
                           
                                @endforeach
                             </tbody>
                        </table>

                    @endif
                </div>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
        <script>window.print();</script>
    </body>
    </html>