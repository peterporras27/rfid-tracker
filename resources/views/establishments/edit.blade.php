<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Edit Establishment') }}
        </h2>
    </x-slot>

    <div class="container mt-5">

         @if (session('success'))

            <div class="alert alert-success mb-2 mt-2" role="alert">
                <h4 class="alert-heading">Success!</h4>
                <p>{{ session('success') }}</p>
            </div>

        @endif

        <x-auth-validation-errors class="mb-2" :errors="$errors" />

        <div class="row justify-content-center">

            <div class="col-5">

                <div class="card">
                    <div class="card-body">
                        <form method="POST" action="{{ route('establishments.update', $establishment->id) }}">
                            @csrf
                            <div class="mb-3">
                                <label for="name" class="form-label">Name</label>
                                <input type="text" name="name" class="form-control" id="name" value="{{ $establishment->name }}">
                            </div>

                            <div class="mb-3">
                                <label for="address" class="form-label">Address</label>
                                <input type="text" name="address" class="form-control" id="address" value="{{ $establishment->address }}">
                            </div>
                            <div class="mb-3">
                                <label for="phone" class="form-label">Phone</label>
                                <input type="text" name="phone" class="form-control" id="phone" value="{{ $establishment->phone }}">
                            </div>

                            <div class="mb-3">
                                <label for="device_id" class="form-label">Device ID</label>
                                <input type="text" name="device_id" class="form-control" id="device_id" value="{{ $establishment->device_id }}">
                            </div>

                            <button type="submit" class="btn btn-primary float-start">Save</button>
                            
                        </form>
                        <form method="POST" action="{{ route('establishments.destroy', $establishment->id) }}">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="btn btn-danger float-end">Delete</button>
                        </form>
                        

                    </div>
                </div>

            </div>
        </div>
    </div>
</x-app-layout>
