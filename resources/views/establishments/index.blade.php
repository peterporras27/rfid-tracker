<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Establishments') }} <a class="btn btn-success btn-sm float-end" href="{{route('establishments.create')}}" role="button">Add Establishment +</a>
        </h2>
    </x-slot>

    <div class="container mt-5">

         @if (session('success'))

        <div class="alert alert-success mb-2 mt-2" role="alert">
            <h4 class="alert-heading">Success!</h4>
            <p>{{ session('success') }}</p>
        </div>

        @endif
        
        <div class="row">

            @foreach( $establishments as $est )
            <div class="col-3">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">{{ $est->name }}</h5>
                        <p class="card-text"><b>Address:</b> {{ $est->address }} <br><b>Phone:</b> {{ $est->phone }}</p>
                        <a href="{{ route('establishments.show', $est->id) }}" class="btn btn-primary btn-sm">View Logs</a>
                        <a href="{{ route('establishments.edit', $est->id) }}" class="btn btn-secondary btn-sm">Edit</a>
                    </div>
                </div>
            </div>
            @endforeach

        </div>
        <hr>
        <div class="row">
            <div class="col">
                {!! $establishments->render() !!}
            </div>
        </div>
    </div>
</x-app-layout>
