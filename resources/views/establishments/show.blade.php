<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ $establishment->name.' '.__('Logs') }}
        </h2>
    </x-slot>

    <div class="container mt-5">

         @if (session('success'))

        <div class="alert alert-success mb-2 mt-2" role="alert">
            <h4 class="alert-heading">Success!</h4>
            <p>{{ session('success') }}</p>
        </div>

        @endif
        
        <div class="row">

            <form method="get" class="mb-2">
                <div class="row">
                    <div class="col">
                        <div class="form-floating mb-3">
                            <input type="date" name="from" class="form-control" id="floatingInput" value="{{ request()->get('from') }}">
                            <label for="floatingInput">From:</label>
                        </div>
                    </div>
                     <div class="col">
                        <div class="form-floating mb-3">
                            <input type="date" name="to" class="form-control" id="floatingInput" value="{{ request()->get('to') }}">
                            <label for="floatingInput">To:</label>
                        </div>
                    </div>
                    <div class="col-2">
                        <button type="submit" class="btn btn-primary btn-lg mt-1">Filter</button>
                        <a target="_blank" href="{{ route('establishments.print',['id'=>$establishment->id,'from'=>request()->get('from'),'to'=>request()->get('to')]) }}" 
                            class="btn-success btn mb-3 float-end btn-lg mt-1">Print</a>
                    </div>
                </div>
            </form>

            <div class="card">
                    <div class="card-body">

                        @if($logs->count())

                        <table id="print" class="table table-bordered table-striped table-hover table-sm">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>RFID</th>
                                    <th>Date</th>
                                    <th>Time</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                @foreach( $logs as $log )

                                <tr>
                                    <td>{{ $log->name }}</td>
                                    <td>{{ $log->rfid }}</td>
                                    <td>{{ $log->created_at->format('F j, Y') }}</td>
                                    <td>{{ $log->created_at->format('g:i a') }}</td>
                                 </tr>
                           
                                @endforeach
                             </tbody>
                        </table>

                        {!! $logs->render() !!}

                        @else

                        <div class="alert alert-info" role="alert">
                          <h4 class="alert-heading">Oops!</h4>
                          <p>There are no available logs to show at the moment.</p>
                        </div>

                        @endif

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
