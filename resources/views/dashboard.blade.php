<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Establishments') }} <a class="btn btn-success btn-sm float-end" href="{{route('establishments.create')}}" role="button">Add Establishment +</a>
        </h2>
    </x-slot>

    <div class="container mt-5">
        <div class="row">
            <div class="col-3">
                
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">7/11 Convinient Store</h5>
                        <p class="card-text"><b>Address:</b> San Jose Ward, Pototan, Iloilo City <br><b>Phone:</b> 09568555540</p>
                        <a href="#" class="btn btn-primary">View Logs</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</x-app-layout>
