<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Member List') }} <a class="btn btn-success btn-sm float-end" href="{{route('members.create')}}" role="button">Add Member +</a>
        </h2>
    </x-slot>

    <div class="container mt-5">

         @if (session('success'))

        <div class="alert alert-success mb-2 mt-2" role="alert">
            <h4 class="alert-heading">Success!</h4>
            <p>{{ session('success') }}</p>
        </div>

        @endif
        
        <div class="row">

            <div class="card">
                    <div class="card-body">

                        <table class="table table-bordered table-striped table-hover table-sm">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Age</th>
                                    <th>Gender</th>
                                    <th>Adress</th>
                                    <th>Phone</th>
                                    <th>RFID</th>
                                    <th>Option</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                @foreach( $members as $mem )

                                <tr>
                                    <td>{{ $mem->first_name.' '.$mem->middle_name.' '.$mem->last_name }}</td>
                                    <td>{{ $mem->age }}</td>
                                    <td>{{ $mem->gender }}</td>
                                    <td>{{ $mem->address }}</td>
                                    <td>{{ $mem->phone }}</td>
                                    <td>{{ $mem->rfid }}</td>
                                    <td>
                                        <a href="{{ route('members.edit', $mem->id) }}" class="btn btn-secondary btn-sm">Edit</a>
                                        <form method="POST" class="float-end" action="{{ route('members.destroy', $mem->id) }}">
                                            @method('DELETE')
                                            @csrf
                                            <button type="submit" class="btn btn-danger btn-sm float-end">Delete</button>
                                        </form>
                                    </td>
                                 </tr>
                           
                                @endforeach
                             </tbody>
                        </table>

                        {!! $members->render() !!}
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
