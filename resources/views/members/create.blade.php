<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Register New Member') }}
        </h2>
    </x-slot>

    <div class="container mt-5">

        @if (session('success'))

            <div class="alert alert-success mb-2 mt-2" role="alert">
                <h4 class="alert-heading">Success!</h4>
                <p>{{ session('success') }}</p>
            </div>

        @endif
        
        <x-auth-validation-errors class="mb-2" :errors="$errors" />

        <div class="row justify-content-center">

            <div class="col">
                
                <div class="card">
                    <div class="card-body">
                        <form method="POST" action="{{ route('members.store') }}">
                            @csrf

                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label for="first_name" class="form-label">First Name</label>
                                        <input type="text" name="first_name" class="form-control" id="first_name" value="{{ old('first_name') }}">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="mb-3">
                                        <label for="last_name" class="form-label">Last Name</label>
                                        <input type="text" name="last_name" class="form-control" id="last_name" value="{{ old('last_name') }}">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                 <div class="col">
                                    <div class="mb-3">
                                        <label for="middle_name" class="form-label">Middle Name</label>
                                        <input type="text" name="middle_name" class="form-control" id="middle_name" value="{{ old('middle_name') }}">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="mb-3">
                                        <label for="rfid" class="form-label">RFID</label>
                                        <input type="text" name="rfid" class="form-control" id="rfid" value="{{ old('rfid') }}">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-3">
                                    <div class="mb-3">
                                        <label for="age" class="form-label">Age:</label>
                                        <input type="number" name="age" class="form-control" id="age" value="{{ old('age') }}">
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="mb-3">
                                        <label for="gender" class="form-label">Gender</label>
                                        <select name="gender" class="form-control" id="gender">
                                            <option value="Male"{{ old('gender') == 'Male' ? ' selected':'' }}>Male</option>
                                            <option value="Female"{{ old('gender') == 'Female' ? ' selected':'' }}>Female</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="mb-3">
                                        <label for="address" class="form-label">Address</label>
                                        <input type="text" name="address" class="form-control" id="adress" value="{{ old('adress') }}">
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="mb-3">
                                        <label for="phone" class="form-label">Phone</label>
                                        <input type="text" name="phone" class="form-control" id="phone" value="{{ old('phone') }}">
                                    </div>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
</x-app-layout>
