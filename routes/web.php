<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MembersController;
use App\Http\Controllers\EstablishmentsController;
use App\Http\Controllers\ApiController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Landing Page / Home Page
Route::get('/', function () {
    // Redirect to dashboard / homepage
    return redirect('dashboard');
});

Route::get('/test', function () {
    // Redirect to dashboard / homepage
    return view('send');
});


// API for Fetching RFID Scanners from establishments.
Route::post('/api', [ApiController::class,'index'])->name('api');

// Default admin homepage.
Route::get('/dashboard', [EstablishmentsController::class,'index'])->name('dashboard');

// All URLS partaining to Establishments.
Route::controller(EstablishmentsController::class)->group(function () {
    // Display landing page
    Route::get('/establishments', 'index')->name('establishments');
    // Display Create form page
    Route::get('/establishments/create', 'create')->name('establishments.create');
    // Process submitted information
    Route::post('/establishments', 'store')->name('establishments.store');
    // Display information
    Route::get('/establishments/{id}', 'show')->name('establishments.show');
    // Display Edit information
    Route::get('/establishments/{id}/edit', 'edit')->name('establishments.edit');
    // Update Submitted Information.
    Route::post('/establishments/{id}/update', 'update')->name('establishments.update');
    // Remove Saved Information/
    Route::delete('/establishments/{id}/destroy', 'destroy')->name('establishments.destroy');
    // Print Logs/
    Route::get('/establishments/{id}/print', 'print')->name('establishments.print');
});

Route::controller(MembersController::class)->group(function () {
    Route::get('/members', 'index')->name('members');
    Route::get('/members/create', 'create')->name('members.create');
    Route::post('/members', 'store')->name('members.store');
    Route::get('/members/{id}', 'show')->name('members.show');
    Route::get('/members/{id}/edit', 'edit')->name('members.edit');
    Route::post('/members/{id}/update', 'update')->name('members.update');
    Route::delete('/members/{id}/destroy', 'destroy')->name('members.destroy');
});

require __DIR__.'/auth.php';
